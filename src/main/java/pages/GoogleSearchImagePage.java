package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;

import java.util.List;

public class GoogleSearchImagePage extends BasePage{

    //@FindBys(@FindBy(xpath = "//div[contains(@class,'isv-r')]"))
    @FindBys(@FindBy(tagName = "img"))
    List<WebElement> imageListActual;

    protected GoogleSearchImagePage(WebDriver driver) {
        super(driver);
    }

    public int countResultImages() {
        return imageListActual.size();
    }
}
