package pages;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class GoogleSearchPage extends BasePage {

    private static final String HOMEPAGE_URL = "https://google.com/";

    @FindBy(name = "q")
    private WebElement searchInput;

    @FindBy(xpath = "//div[@class='FPdoLc lJ9FBc']//input[@name='btnK']")
    private WebElement searchButton;

    public GoogleSearchPage(WebDriver driver) {
        super(driver);
    }

    public GoogleSearchPage openPage() {
        driver.get(HOMEPAGE_URL);
        this.waitVisibilityOfElement(50, searchInput);
        return this;
    }

    public GoogleSearchResultPage search(String term) {
        searchInput.sendKeys(term);
        searchInput.sendKeys(Keys.ENTER);
        return new GoogleSearchResultPage(driver);
    }
}
