package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class GoogleSearchResultPage extends BasePage{

    @FindBy(xpath = "//a[@data-hveid='CAEQBA']")
    private WebElement imageButton;

    public GoogleSearchResultPage (WebDriver driver) {
        super(driver);
    }

    public GoogleSearchImagePage openImagesTab() {
        this.waitVisibilityOfElement(50, imageButton);
        imageButton.click();
        return new GoogleSearchImagePage(driver);
    }
}
